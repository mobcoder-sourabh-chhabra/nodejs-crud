let chai = require("chai");
let chaiHttp = require("chai-http");

let server = require("../server");
const Mocha = require("mocha");
chai.should();

chai.use(chaiHttp);



describe("Tasks API", () => {
  describe(" /user/Login", () => {
    it("It should Login  the  User", (done) => {
      const task = {
        userEmail: "schhabra3901@gmail.com",
                password: "test1234"

      };
      chai
        .request(server)

        .post("/user/Login/")
        .send(task)
        .end((err, response) => {
          if (err) return done(err);
          // res.body.should.be("Hello World!");

          response.should.have.status(200);
          // response.text.should.be.eq("The userEmail property should be entered")
          done();
        });
    });
  });


  describe("GET User Profile", () => {
    it("should get user profile for valid token", (done) => {
      const userId = "641d2ecdb796761d0fa5523f";
      const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NDFkMmVjZGI3OTY3NjFkMGZhNTUyM2YiLCJ1c2VyRW1haWwiOiJzY2hoYWJyYTM5MDFAZ21haWwuY29tIiwiaWF0IjoxNjc5OTE3MDcyLCJleHAiOjE2ODAwMDM0NzJ9.5L8HseIo06_-xmbG1-hyFAjlQ4mleBuTjsB99oKnPGE";
      chai.request(server)
        .get("/user/getUserProfile")
        .set("authorization", token)
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('Data');
            res.body.Data.should.have.property('_id').eql(userId);
            done();
        });
    });
 
  });
 
 
    describe(" /user/change-password", () => {
      it("It should Change  the password of  User", (done) => {
        const task = {
         
                  password: "test1234"
  
        };
        chai
          .request(server)
  
          .put("/user/change-password/")
          .set("authorization",'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NDFkMmVjZGI3OTY3NjFkMGZhNTUyM2YiLCJ1c2VyRW1haWwiOiJzY2hoYWJyYTM5MDFAZ21haWwuY29tIiwiaWF0IjoxNjc5OTE3MDcyLCJleHAiOjE2ODAwMDM0NzJ9.5L8HseIo06_-xmbG1-hyFAjlQ4mleBuTjsB99oKnPGE' )
          .send(task)
          .end((err, response) => {
            if (err) return done(err);
            // res.body.should.be("Hello World!");
  
            response.should.have.status(200);
            // response.text.should.be.eq("The userEmail property should be entered")
            done();
          });
      });
    });


  describe("Update Email", () => {
    it("should Update The Email Of User", (done) => {
      const task = {
       
        userEmail: "schhabra3901@gmail.com"

      };
      chai
        .request(server)

        .put("/user/updateEmail/")
        .set("authorization",'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NDFkMmVjZGI3OTY3NjFkMGZhNTUyM2YiLCJ1c2VyRW1haWwiOiJzY2hoYWJyYTM5MDFAZ21haWwuY29tIiwiaWF0IjoxNjc5OTE3MDcyLCJleHAiOjE2ODAwMDM0NzJ9.5L8HseIo06_-xmbG1-hyFAjlQ4mleBuTjsB99oKnPGE' )
        .send(task)
        .end((err, response) => {
          if (err) return done(err);
          // res.body.should.be("Hello World!");

          response.should.have.status(200);
          // response.text.should.be.eq("The userEmail property should be entered")
          done();
        });
    });
  });
});

