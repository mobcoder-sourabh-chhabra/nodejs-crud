const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Userss = require("../models/userModel");

const { check, validationResult } = require("express-validator");

exports.userRegister = [ 
  check("userEmail", "Please enter a valid email").isEmail(),
  check("password", "Please enter a valid password").isLength({ min: 5 }),
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    bcrypt.hash(req.body.password, 10, (err, hash) => {
      if (err) {
        console.log(err);
        return res.status(500).json({
          error: err,
        });
      } else {
        const Users = new Userss({
          userEmail: req.body.userEmail,
          password: hash,
        });

        Users.save()
          .then((result) => {
            res.status(200).json({
              Data: result,
            });
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({
              msg: "User already exist",
              error: err,
            });
          });
      }
    });
  },
];


exports.userLogin = [
  check("userEmail", "Please enter valid email").isEmail(),
  check("password", "Please enter valid password").not().isEmpty(),
  (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
      return res.status(400).json({
        msg: error.array()[0].msg,
      });
    }
    Userss.find({ userEmail: req.body.userEmail })
      .exec()
      .then((user) => {
        if (user.length < 1) {
          return res.status(401).json({
            msg: "User Not Exist",
          });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
          if (!result) {
            return res.status(401).json({
              msg: "password Matching Fail",
            });
          }
          if (result) {
            console.log("pppppp", user[0]._id);
            const token = jwt.sign(
              {
                userId: user[0]._id,
                userEmail: user[0].userEmail,
              },
              process.env.SecretKey,
              {
                expiresIn: "24h",
              }
            );

            res.status(200).json({
              userEmail: user[0].userEmail,
              token: token,
              userId: user[0]._id,
            });
          }
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  },
];

exports.getUserProfile = (req, res, next) => {
  const userId = req.userData.userId;

  Userss.findById({ _id: userId })
    .exec()
    .then((result) => {
      res.status(200).json({
        Data: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};
exports.userChangePassword = async (req, res) => {
  try {
    const userId = req.userData.userId;

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    const userPassword = await Userss.findByIdAndUpdate(
      { _id: userId },
      { password: password },
      { new: true }
    );
    return res.status(200).json({
      status: true,
      data: userPassword,
    });
  } catch (err) {
    console.log("8888888888", err);
    return res.status(400).json({
      status: false,
      error: "Error Occured",
    });
  }
};

exports.updateEmail = (req, res) => {
  const userId = req.userData.userId;

  Userss.findOneAndUpdate(
    { _id: userId },
    {
      $set: {
        userEmail: req.body.userEmail,
      },
    }
  )
    .then((result) => {
      res.status(200).json({
        Data: result,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

exports.deleteAccount = (req, res) => {
  const userId = req.userData.userId;
  console.log("pppppppppp", userId);
  Userss.remove({ _id: userId })
    .then((result) => {
      res.status(200).json({
        Result: result,
      });
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
};
