const express = require("express");
const app = express();
const router = express.Router();
const checkAuth = require("../../middleware/check-auth");

const {
  userRegister,
  userLogin,
  getUserProfile,
  userChangePassword,
  updateEmail,
  deleteAccount
} = require("../controllers/userController");

router.post("/register", userRegister);

router.post("/Login", userLogin);

router.get("/getUserProfile", checkAuth, getUserProfile);
router.put('/change-password',checkAuth,userChangePassword)
router.put('/updateEmail',checkAuth,updateEmail)
router.delete("/deleteAccount",checkAuth,deleteAccount)


module.exports = router;
