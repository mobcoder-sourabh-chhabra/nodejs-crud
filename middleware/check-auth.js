const jwt = require('jsonwebtoken')

module.exports = (req, res, next)=>{

try{
const token = req.headers.authorization 
console.log(token)
const verify = jwt.verify(token, process.env.SecretKey);
console.log(verify);
req.userData = verify;
next();
}
catch(err)
{
   return res.status(401).json({
        msg: "Invalid Token"
    })
}
 
}