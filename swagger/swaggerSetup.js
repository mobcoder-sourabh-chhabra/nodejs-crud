const swaggerUi = require("swagger-ui-express");
const { SwaggerTheme } = require("swagger-themes");
const swaggerDocument = require("./swagger");

const theme = new SwaggerTheme("v3");

const options = {
  explorer: true,
  customCss: theme.getBuffer("dark"),
};

function swagger(app) {
  app.use(
    "/apiDocs/v1",
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, options)
  );
}

module.exports = swagger;
